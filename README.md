# RasPI Speed Game

RasPI Speed Game is speed game designed to be run on RaspBerry PI. The game blinks four leds at random and the player needs to press corresponding keys on the keyboard (1,2,3,4). The game continues and gets faster until the player makes a mistake. Players score is saved to a text file and top ten scores are presented to the player on the terminal.

## Getting started

What you'll need:

  - RaspBerry PI
  - Four leds

(This script has been tested on a RaspBerry PI 2 model B with LinkSprite Linker kit Base shield and four LinkSprite leds)


Connect the leds to GPIO pins 20, 21, 25 and 26. (The corresponding keyboard keys are: 26=1, 25=2, 21=3, 20=4)
Connect to your RaspBerry, open Terminal, navigate to the right directory and run:


```sh
$ python3 nopeuspeli.py
```


## Gameplay

The game starts with a countdown where all leds are blinked. Press keys 1,2,3,4 as the corresponding leds are blinked. There is no time bound to pressing the buttons, they just need to be pressed in the same order as the leds are blinked. The game ends when the player makes a mistake. The player is asked to input a three character initials. The player's score is saved to leaderboard.txt and the top ten scores are displayed on the terminal.


## What I learned

This was a school project for a class in programming with RaspBerry PI in spring 2018. I learned about the process of designing a software that uses some elements I haven't used previously. I cut the game down to its core components and first made tests and practised so that all the components worked individually and finally I put them all together and made some necessary adjustments. In order to detect single key presses I used the Tkinter library which I have used extensively in a later project. I learned about threading which allowed blinking the leds while simultaneously listening the user input. I also learned about reading and writing to files. One challenge was to read the leaderboard file and only print the top ten best scores in order. 