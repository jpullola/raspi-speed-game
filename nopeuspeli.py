from random import randrange
import RPi.GPIO as GPIO
import time
import tkinter as tk
import threading


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(26, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
GPIO.setup(20, GPIO.OUT)


pituus = 1000
oikeatNumerot = [0]*pituus
pelaajanNumerot=[0]*pituus
num = 0
error = 0
name = "example_name"



for i in range(0, pituus):
	sameAsPrevious = 0
	while sameAsPrevious == 0:
		b = randrange(1, 5)
		if b != oikeatNumerot[(i-1)]:
			oikeatNumerot[i] = b
			sameAsPrevious = 1


def lediVilkku():
	global error

	print("Peli alkaa 5 sekunnin kuluttua...") #Uncomment for finnish
	#print("The game starts in 5 seconds...") #Uncomment for english
	GPIO.output(26, True)
	time.sleep(1)
	GPIO.output(25, True)
	time.sleep(1)
	GPIO.output(21, True)
	time.sleep(1)
	GPIO.output(20, True)
	time.sleep(1)
	GPIO.output(26, False)
	GPIO.output(25, False)
	GPIO.output(21, False)
	GPIO.output(20, False)
	print("START")
	print()
	time.sleep(1)

	a = 0
	onTime = 0.6
	offTime = 0.6
	while a<len(oikeatNumerot):
		oikea = oikeatNumerot[a]
		if oikea == 1:
			pinni = 26
		if oikea == 2:
			pinni = 25
		if oikea == 3:
			pinni = 21
		if oikea == 4:
			pinni = 20
		GPIO.setup(pinni, GPIO.OUT)
		GPIO.output(pinni, True)
		time.sleep(onTime)
		GPIO.output(pinni, False)
		time.sleep(offTime)
		a+=1
		if 0<a<50:
			onTime = onTime*0.987
			offTime = offTime *0.987
		if 50<a<100:
			onTime = onTime*0.997
			offTime = offTime *0.997
		if 100<a<200:
			onTime = onTime*0.995
			offTime = offTime *0.995
		if 200<a<500:
			onTime = onTime*0.998
			offTime = offTime *0.998
		if a<500:
			ontime = onTime*0.9999
			offTime = offTime*0.9999
		if error == 1:
			a = 99999
	


def onKeyPress(event):
	global num
	global pituus
	global error
	if event.char.isdigit():
		if 1<=int(event.char)<=4:
			pelaajanNumerot[num] = int(event.char)
			if pelaajanNumerot[num] != oikeatNumerot[num]:
				print("Peli ohi, sait", num, "pistett�") #Uncomment for finnish
				#print("Game over. You got", num, "points") #Uncomment for english
				error = 1
				root.destroy()
			if pelaajanNumerot[num] == oikeatNumerot[num]:
				num+=1
		if num == pituus:
			print("VOITIT PELIN, sait kaikki oikein") #Uncomment for finnish
			#print("You win, you got everything correct") #Uncomment for english
			root.destroy()


root = tk.Tk()
root.bind('<KeyPress>', onKeyPress)


def gameOver():
	global name
	while len(name) != 3:
		GPIO.output(26, True)
		GPIO.output(25, True)
		GPIO.output(21, True)
		GPIO.output(20, True)
		time.sleep(0.5)
		GPIO.output(26, False)
		GPIO.output(25, False)
		GPIO.output(21, False)
		GPIO.output(20, False)
		time.sleep(0.5)


thread_1 = threading.Thread(target=lediVilkku)
thread_1.start()
thread_2 = threading.Thread(target=root.mainloop())
thread_2.start()
thread_3 = threading.Thread(target=gameOver)


def writeScoreToFile():
	global num
	global name
	thread_3.start()
	while len(name) != 3:
		name = input("Anna nimikirjaimesi (3 merkki�): ") #Uncomment for finnish
		#name = input("Enter your initials (3 characters): ") #Uncomment for english
	leaderBoard = open("leaderboard.txt", "a")
	leaderBoard.write(name)
	leaderBoard.write("\n")
	leaderBoard.write(str(num))
	leaderBoard.write("\n")
	leaderBoard.close()

def printLeaderboard():
	alist = [line.rstrip() for line in open('leaderboard.txt')]
	blist = [[] for x in range(len(alist)//2)]
	i=0
	a=0
	while i < len(alist):
		blist[a].append(alist[i])
		blist[a].append(int(alist[i+1]))
		i+=2
		a+=1
	
	blist.sort(key=lambda x: x[1], reverse=True)
	if len(blist) >=10:
		print()
		print("Top 10 tulokset:") #Uncomment for finnish
		#print("Top 10 scores:") #Uncomment for english
		for topTen in range(0, 10):
			if topTen < 9:
				print(" ", sep="", end="")
			print(topTen+1, ": ", sep="", end="")
			print(blist[topTen][0], end=" - ")
			print(blist[topTen][1])
	else:
		print()
		print("Parhaat tulokset:") #Uncomment for finnish
		#print("Top scores:") #Uncomment for english
		for topTen in range(0, len(blist)):
			if topTen < 9:
				print(" ", sep="", end="")
			print(topTen+1, ": ", sep="", end="")
			print(blist[topTen][0], end=" - ")
			print(blist[topTen][1])



writeScoreToFile()
printLeaderboard()